![](ressources/logo.jpeg)

# TP Démarrage du projet IHM - _Trains_

Ce TP est prévu pour vous aider à comprendre comment interagir avec la mécanique interne du jeu. **Attention, ce n'est pas le dépôt correspondant à votre projet**, mais un dépôt correspondant à un travail préparatoire. Chaque membre de l'équipe de projet travaille dans son propre dépôt, afin de se familiariser individuellement avec l'environnement. La durée estimative du TP est de **1h-1h30**.

Vous exploiterez ensuite le travail que vous avez fait pendant ce TP, pour l'intégrer (éventuellement en copiant-collant) dans les fichiers de votre projet. Le code de la mécanique de ce TP est totalement identique à celui que vous avez dans le dépôt de votre projet IHM.

Pour vous aider, pensez à consulter le [diagramme de classes](ressources/DiagrammeDeClasses.png).

## Étapes
1. Transformez la vue du jeu en `VBox`.

2. Ajoutez un label `instruction`, et faites en sorte que le texte de ce label corresponde (binding !) à l'instruction donnée par le jeu.
Pour des questions d'affichage correct des informations, vous pouvez temporairement modifier la largeur de la scène pour la fixer à 400, par exemple, dans la classe `TrainsIHM`, ainsi que changer le style du texte de ce label pour choisir une fonte de caractères plus grande.
Vous pouvez aussi rendre transparent les contours des tuiles.

   **Remarque** : tous les bindings ou autres attachements de gestionnaires que vous avez faits ou allez faire dans ce TP seront fait dans la méthode `void creerBindings()`.

3. Dans la classe `VuePlateau`, modifiez le code de la méthode `void choixTuile(MouseEvent event)` qui indique qu'une tuile a été choisie, pour qu'elle transmette cette information au jeu. Il faut penser à transmettre le numéro de la tuile choisie, que l'on obtient avec `getId()`.

   Vous pouvez remarquer qu'une tuile est un `Group` de formes (hexagone pour la tuile, cercle pour les pions des joueurs, et rectangle pour les gares).

4. Actuellement, le jeu est lancé pour 2 joueurs. Pour l'instant, rien ne se voit au niveau de l'IHM (pas encore traité), mais un message est affiché dans la console. La première instruction indique qu'il faut choisir la position du 1er joueur. Si l'information "_une tuile a été choisie_" est correctement transmise au jeu, cette instruction reste la même pour le 2ᵉ joueur, et ce n'est que lorsque vous avez choisi la position de départ du second que l'instruction devient "_Choisissez une action ou passez_".

   Définissez un gestionnaire de changement (dans la classe `VueDuJeu`), qui fera afficher le nom du joueur dans un label `nomJoueur`, et attachez-le à la propriété `joueurCourant`.

5. Ajoutez un bouton `passer`, qui indiquera au jeu qu'on souhaite passer (lorsqu'on cliquera sur le bouton).

6. On va maintenant faire apparaitre les cartes en main du joueur courant. Pour cela, ajoutez dans la classe `VueDuJeu` une `HBox` qui contiendra autant de boutons que de cartes en main. Ajoutez un écouteur (sur la propriété correspondante de `IJeu`), qui, lorsque le joueur courant change, vide la liste des enfants de cette `HBox`, puis parcourt la liste des cartes en main du joueur courant pour créer (et ajouter) un bouton par carte. Le texte du bouton correspond au nom de la carte.

7. Sur chacun des boutons créés à la question précédente, ajoutez un gestionnaire de clic qui informe le jeu qu'une carte de la main du joueur a été choisie. Pour l'instant rien ne se voit sur l'IHM, mais si vous passez jusqu'à pouvoir choisir une gare, vous devriez voir que l'instruction indique qu'il faut choisir la tuile où poser la carte.

8. Écrire un écouteur de changement, à exécuter quand la liste des cartes en main change : lorsqu'une carte est enlevée de la main, il faut enlever le bouton correspondant dans la `HBox`. Pour découper le travail, commencez par écrire une méthode `Button trouverBoutonCarte(Carte carteATrouver)` qui trouve le premier bouton dont le texte correspond à celui de la `Carte`.

   Attachez ce même gestionnaire à la propriété "_main_" de chacun des joueurs du jeu.

9. On va maintenant matérialiser sur l'IHM la pose d'un rail. Pour cela, dans la méthode `void ajouteRailATuile(Tuile t, Joueur j, Circle c)` de la classe `VuePlateau`, supprimez le code qui affiche un message dans la console, et remplacez-le par le code qui remplit le cercle (passé en paramètres) avec la couleur du joueur. Pour cela, remarquez la classe utilitaire `CouleursJoueurs` qui contient une `Map` faisant correspondre les couleurs aux `String` en format hexadecimal. Par la suite, vous pourrez ajouter une image à la place de cette couleur.

Étapes suivantes (à faire éventuellement en groupe de projet) :
- on peut continuer sur la `VueCarte`, et transformer la liste des cartes en main du joueur courant de façon à utiliser cette vue.
- on peut continuer sur la `VueJoueurCourant` et déplacer certaines parties du code précédent dans cette nouvelle vue (changement de la main par exemple).

